describe('Teste no blog do Agi', () => {
  beforeEach(() => {
    cy.visit('https://blogdoagi.com.br')
  })
   
  it('Testando busca do blog', () => {
    const searchInput = "#search-field"
    const toSearch = "teste"
    const title = ".page-title.ast-archive-title"
    const expectedText = `Search Results for: ${toSearch}`

    const search = cy.get('.astra-search-icon').first()

    search.click()

    cy.get(searchInput).should("be.visible")
    cy.get(searchInput).type(`${toSearch}{enter}`)
    cy.get(title).should(($title) => {
      expect($title.first()).to.contain(expectedText)  
    })
  })
  
  it('Testando cadastro na newsletter', () => {
    const nameInput = ".uagb-forms-name-input"
    const emailInput = ".uagb-forms-email-input"
    const submitButton = ".uagb-forms-main-submit-button"
    const name = "Renato Fernandes da Nóbrega"
    const email = "rnobrega32@gmail.com"

    const messageField = ".uagb-forms-success-message"
    const expectedText = 'Obrigado! Agora você receberá todas as novidades por e-mail.'
    
    cy.get(nameInput).type(name)
    cy.get(emailInput).type(email)
    cy.get(submitButton).click()

    cy.get(messageField).should($messageField => {
      expect($messageField.first()).to.contain(expectedText)  
    })
  })

  it('Testando menu', () => {
    const menu = "#ast-hf-menu-1"
    const title = ".page-title.ast-archive-title"
    const expectedText = 'Colunas'
    cy.get(menu).get("li").first().realHover("mouse")
    cy.get(menu).get("li").first().realHover("mouse").find("a").eq(1).click()

    cy.get(title).should(($title) => {
      expect($title.first()).to.contain(expectedText)  
    })
    
  })
  
})