


# Teste blog Agi

Teste do blog Agi(https://blogdoagi.com.br) utilizando Cypress


## Requisitos
- Node
- pnpm (ou npm)
- Cypress


## Execução

Clone o projeto

```bash
  git clone https://gitlab.com/renatonobrega1/teste-agi.git
```

Entre no diretório do projeto

```bash
  cd teste-agi
```

Instale as dependências

```bash
  pnpm install
```
ou
```bash
  npm install
```

Execute os testes

```bash
  ./node_module/.bin/cypress open
```


## Melhorias

Adicionar ids nos inputs

